# Free data

If you know any free data like:
- Website to scrap
- An open API
- Database dump

and more, you share by [creating an issue](https://gitlab.com/lurlo/freedata/issues)
